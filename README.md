# Twitter Hashtag Suggestion Project

## Authors 
Joel Kemp and Wai Khoo

## Goal
As you're typing a tweet, the system suggests appropriate hashtags based on the content of the tweet and the content of your previous tweets.

## Description
This system is an experiment to suggest hashtags for a user's current tweet. The system pulls the backlog of tweets for the current user and performs a data-mining analysis of those tweets to suggest appropriate, dominant terms/categories (i.e., hashtags).

## Detail
We used a subset of Ashton's tweets as input data and used kmean for training the data.